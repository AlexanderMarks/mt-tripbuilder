$(document).ready(function () {
	// Initialize JqueryUI elements
	$("#main, #create-trip").tabs({activate: function() {$("#error").css("display", "none"); $("#error").html("");}});
	$(":text").datepicker({dateFormat: 'dd-mm-yy'});
	
	// Load Trips Table
	$.ajax({
		type: "GET",
		url: "../app/getTrips",
		success: function (response) {
			$.each(response, function (index, value) {
				
				$("#trip-table tbody").append("<tr><td>"+value.total_price
				+"</td><td>"+value.flight_count
				+"</td><td>"+value.initial_departure
				+"</td><td><a href='#trip-details-"+index+"' data-toggle='collapse' role='button' class='btn btn-primary'>view</td></tr>"
				+"<tr id='trip-details-"+index+"' class='collapse'><td colspan=4></td></tr>");
				
				$.ajax({
					type: "GET",
					url: "../app/getTripDetails",
					data: "id="+value.tripid,
					success: function (response) {
						$("#trip-details-"+index+" td").append(
							"<table><thead><tr>"
							+"<th>Airline</th>"
							+"<th>Number</th>"
							+"<th>Departure Date</th>"
							+"<th>Departure Airport</th>"
							+"<th>Departure City</th>"
							+"<th>Departure Time</th>"
							+"<th>Departure Timezone</th>"
							+"<th>Arrival Airport</th>"
							+"<th>Arrival City</th>"
							+"<th>Arrival Timezone</th>"
							+"<th>Price</th><thead><tbody></tbody></table>");
						$.each(response, function (detailIndex, detailValue) {
							$("#trip-details-"+index+" td table tbody").append(
								"<tr><td>"+detailValue.airline+"</td>"
								+"<td>"+detailValue.number+"</td>"
								+"<td>"+detailValue.departure_date+"</td>"
								+"<td>"+detailValue.departure_name+"</td>"
								+"<td>"+detailValue.departure_city+"</td>"
								+"<td>"+detailValue.departure_time+"</td>"
								+"<td>"+detailValue.departure_timezone+"</td>"
								+"<td>"+detailValue.arrival_name+"</td>"
								+"<td>"+detailValue.arrival_city+"</td>"
								+"<td>"+detailValue.arrival_timezone+"</td>"
								+"<td>"+detailValue.price+"</td></tr>");
						});
					}
				});
			});
		}
	});
	
	
	setFlights("#one-way-flight, #round-trip-departFlight, #open-jaw-departFlight, #multi-city-flight-0")
	
	$("form").on("submit", function(e) {
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(response) {
				location.reload();
			},
			error: function(response) {
				$("#error").css("display", "block");
				$("#error").html(response.responseText);
			}
		});
	});
	
	$("#round-trip-departFlight").on("change", function (e) {
		var selected = $(this).find("option:selected");
		setFlights("#round-trip-returnFlight", false, "?depart="+selected.attr("arriveCity")+"&arrive="+selected.attr("departCity"));
	});
	
	$("#open-jaw-departFlight").on("change", function (e) {
		var selected = $(this).find("option:selected");
		setFlights("#open-jaw-returnFlight", false, "?departExclude="+selected.attr("arriveCity")+"&arrive="+selected.attr("departCity"));
	});
	
	$("[id^=multi-city-flight-]").on("change", multiCityOnChange);
	
	$("#multi-city-add").on("click", function (e) {
		var rowCount = $("[id^=multi-city-flight-]").length;
		if(rowCount < 5) {
			var prevSelect = $("#multi-city-form .form-group").last();
			prevSelect.after(
				"<div class='form-group row'>"
					+"<label for='multi-city-flight-"+rowCount+"'>Flight "+eval(rowCount+1)+"</label>"
					+"<div class='flight-select'>"
						+"<select id='multi-city-flight-"+rowCount+"' row="+rowCount+" name='flights[]' class='form-control' required></select>"
					+"</div>"
					+"<label for='multi-city-date-"+rowCount+"' >Date "+eval(rowCount+1)+"</label>"
					+"<div style='margin-left: 20px'>"
						+"<input type='text' id='multi-city-date-"+rowCount+"' name='dates[]' class='form-control' autocomplete='off' required>"
					+"</div>"
				+"</div>");
			
			$("#multi-city-flight-"+rowCount).on("change", multiCityOnChange);
			$("#multi-city-date-"+rowCount).datepicker({dateFormat: 'dd-mm-yy'});
			setFlights($("#multi-city-flight-"+rowCount), false, "?depart="+prevSelect.find("option:selected").attr("arriveCity")+"&arriveExclude="+prevSelect.find("option:selected").attr("departCity"));
			if($("#multi-city-remove").css("display") == "none")
				$("#multi-city-remove").show();
			if(rowCount == 4)
				$("#multi-city-add").hide();
		}
	});
	
	$("#multi-city-remove").on("click", function (e) {
		var rowCount = $("[id^=multi-city-flight-]").length;
		if(rowCount > 2) {
			$("#multi-city-form .form-group").last().remove();
			if($("#multi-city-add").css("display") == "none")
				$("#multi-city-add").show();
			if(rowCount == 3)
				$("#multi-city-remove").hide();
		}
	});
});

function setFlights(selector, clearSelect= true, params = "", onComplete = null) {
	
	$.ajax({
		type: "GET",
		url: "../app/getFlights"+params,
		success: function(response) {
			var options = "";
			$.each(response, function (index, value) {
				options += "<option value='"+value.flightid+"' departCity='"+value.departure_city+"' arriveCity='"+value.arrival_city+"'>"+value.airline+value.number+" "+value.departure_city+" to "+value.arrival_city+" $"+value.price;
			});
			$(selector).html(options);
			if(clearSelect)
				$(selector).prop("selectedIndex", -1);
			if(onComplete !== null) 
				onComplete();
		}
	});
}

function multiCityOnChange(e) {
	var row = parseInt($(this).attr('row'))+1;
	if($("#multi-city-flight-"+row).length > 0) {
		var selected = $(this).find("option:selected");
		setFlights($("#multi-city-flight-"+row), false, "?depart="+selected.attr("arriveCity")+"&arriveExclude="+selected.attr("departCity"), function () {
			$("#multi-city-flight-"+row).trigger("change");
		});
		/*if($("#multi-city-flight-"+row).find("option:selected").length > 0) {
			$("#multi-city-flight-"+row).trigger("change");
		}*/
	}
}