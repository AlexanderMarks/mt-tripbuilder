-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 23, 2018 at 10:24 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mt-tripbuilder`
--
CREATE DATABASE IF NOT EXISTS `mt-tripbuilder` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `mt-tripbuilder`;

-- --------------------------------------------------------

--
-- Table structure for table `airlines`
--

CREATE TABLE `airlines` (
  `airlineid` int(5) NOT NULL,
  `code` varchar(3) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `airlines`
--

INSERT INTO `airlines` (`airlineid`, `code`, `name`) VALUES
(1, 'AC', 'Air Canada');

-- --------------------------------------------------------

--
-- Table structure for table `airports`
--

CREATE TABLE `airports` (
  `airportid` int(5) NOT NULL,
  `code` varchar(3) NOT NULL,
  `city_code` varchar(3) NOT NULL,
  `name` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `country_code` varchar(3) NOT NULL,
  `region_code` varchar(3) NOT NULL,
  `latitude` float(10,6) NOT NULL,
  `longitude` float(10,6) NOT NULL,
  `timezone` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `airports`
--

INSERT INTO `airports` (`airportid`, `code`, `city_code`, `name`, `city`, `country_code`, `region_code`, `latitude`, `longitude`, `timezone`) VALUES
(1, 'YUL', 'YMQ', 'Pierre Elliott Trudeau International', 'Montreal', 'CA', 'QC', 45.457714, -73.749908, 'America/Montreal'),
(2, 'YVR', 'YVR', 'Vancouver International', 'Vancouver', 'CA', 'BC', 49.194698, -123.179192, 'America/Vancouver'),
(3, 'YHZ', 'YHZ', 'Halifax / Stanfield International Airport', 'Halifax', 'CA', 'NS', 44.880798, -63.508598, 'America/Halifax'),
(4, 'YOW', 'YOW', 'Ottawa Macdonald-Cartier International Airport', 'Ottawa', 'CA', 'ON', 45.322498, -75.669197, 'America/Toronto');

-- --------------------------------------------------------

--
-- Table structure for table `flights`
--

CREATE TABLE `flights` (
  `flightid` int(5) NOT NULL,
  `airline` varchar(3) NOT NULL,
  `number` varchar(5) NOT NULL,
  `departure_airport` varchar(3) NOT NULL,
  `departure_time` varchar(5) NOT NULL,
  `arrival_airport` varchar(3) NOT NULL,
  `arrival_time` varchar(5) NOT NULL,
  `price` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flights`
--

INSERT INTO `flights` (`flightid`, `airline`, `number`, `departure_airport`, `departure_time`, `arrival_airport`, `arrival_time`, `price`) VALUES
(1, 'AC', '301', 'YUL', '07:35', 'YVR', '10:05', 273.23),
(2, 'AC', '302', 'YVR', '11:30', 'YUL', '19:11', 220.63),
(3, 'AC', '303', 'YHZ', '9:40', 'YVR', '14:40', 209.98),
(4, 'AC', '304', 'YOW', '15:10', 'YUL', '15:50', 173.86),
(5, 'AC', '305', 'YVR', '10:47', 'YHZ', '22:00', 270.23),
(6, 'AC', '306', 'YHZ', '10:30', 'YOW', '11:40', 194.90),
(7, 'AC', '307', 'YOW', '13:40', 'YVR', '15:50', 243.56);

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

CREATE TABLE `trips` (
  `tripid` int(5) NOT NULL,
  `flightid` int(11) NOT NULL,
  `departure_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trips`
--

INSERT INTO `trips` (`tripid`, `flightid`, `departure_date`) VALUES
(1, 5, '2018-09-29'),
(1, 6, '2018-09-25'),
(1, 7, '2018-09-28'),
(2, 1, '2018-09-27'),
(2, 2, '2018-09-29'),
(3, 4, '2018-09-24'),
(3, 6, '2018-09-26'),
(4, 3, '2018-09-30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `airlines`
--
ALTER TABLE `airlines`
  ADD PRIMARY KEY (`airlineid`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `airports`
--
ALTER TABLE `airports`
  ADD PRIMARY KEY (`airportid`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `flights`
--
ALTER TABLE `flights`
  ADD PRIMARY KEY (`flightid`);

--
-- Indexes for table `trips`
--
ALTER TABLE `trips`
  ADD PRIMARY KEY (`tripid`,`flightid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `airlines`
--
ALTER TABLE `airlines`
  MODIFY `airlineid` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `airports`
--
ALTER TABLE `airports`
  MODIFY `airportid` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `flights`
--
ALTER TABLE `flights`
  MODIFY `flightid` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `trips`
--
ALTER TABLE `trips`
  MODIFY `tripid` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
