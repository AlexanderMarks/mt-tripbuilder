<?php

class Airports extends Model
{
	public $airportid;
	public $code;
	public $city_code;
	public $name;
	public $city;
	public $country_code;
	public $region_code;
	public $latitude;
	public $longitude;
	public $timezone;
	
	public function __construct(){
		parent::__construct();
		$this->_PKName = "airportid";
	}
}

?>