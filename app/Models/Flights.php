<?php

class Flights extends Model
{
	public $flightid;
	public $airline;
	public $number;
	public $departure_airport;
	public $departure_time;
	public $arrival_airport;
	public $arrival_time;
	public $price;
	
	public function __construct(){
		parent::__construct();
		$this->_PKName = "flightid";
	}
}

?>