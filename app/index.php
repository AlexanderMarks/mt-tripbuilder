<?php
require_once 'Core/Controller.php';
require_once 'Core/Model.php';
require_once 'Controllers/TripBuilder.php';
header('Access-Control-Allow-Origin: *');

$url = filter_var(rtrim($_SERVER['REQUEST_URI'], '/'), FILTER_SANITIZE_URL);
//echo $url;
$url = substr($url , strrpos($url, '/')+1, strrpos($url, '?') !== false ? (strrpos($url, '?') - strrpos($url, '/') - 1) : strlen($url));
$tripbuilder = new TripBuilder();

switch($url) {
	case 'getFlights':
		header('Content-Type: application/json');
		echo $tripbuilder->getFlights();
		break;
	case 'getTrips':
		header('Content-Type: application/json');
		echo $tripbuilder->getTrips();
		break;
	case 'getTripDetails':
		echo $tripbuilder->getTripDetails();
		break;
	case 'newOneWayTrip':
		$tripbuilder->newOneWayTrip();
		break;
	case 'newRoundTrip':
		$tripbuilder->newRoundTrip();
		break;
	case 'newMultiCityTrip':
		$tripbuilder->newMultiCityTrip();
		break;
	case 'newOpenJawTrip':
		$tripbuilder->newOpenJawTrip();
		break;
	default:
		http_response_code(400);
		echo "invalid command";
}
?>