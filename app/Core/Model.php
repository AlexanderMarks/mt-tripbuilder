<?php

class Model{
	protected $_connection;
    protected $_className = null;
    protected $_joinClause;
    protected $_whereClause;
	protected $_groupBy;
    protected $_orderBy;
    protected $_PKName = 'ID';
	
	public function __construct(PDO $connection = null)
    {
		//database parameters
		$server = 'localhost';
		$DBName = 'mt-tripbuilder';
		$user = 'root';
		$pass = '';
		
        $this->_connection = $connection;
        if ($this->_connection === null) {
            $this->_connection = new PDO("mysql:host=$server;dbname=$DBName", $user, $pass);
            $this->_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
		$this->_className = get_class($this);
    }

	protected function getProps($excludePK = true){
		//extract the deriving class name
		$exclusions = get_class_vars(__CLASS__);//properties from the Model base class to exclude from SQL
        if (!is_array($this->_PKName) && $excludePK) {
            $exclusions[$this->_PKName] = "";
        }
		
        //extract the deriving class properties
        $classProps = [];
		$array = get_object_vars($this);
		foreach ($array as $key => $value) {
			if(!array_key_exists($key, $exclusions))
				$classProps[] = $key;
		}
		return $classProps;
	}
	
	protected function toArray($properties){
        $data = [];
        foreach($properties as $prop)
            $data[$prop] = $this->$prop;
		return $data;
    }

    public function find($ID)
    {
		$selectOne 	= "SELECT * FROM $this->_className WHERE ";
        if(is_array($this->_PKName)) {
            for ($i=0;$i<count($this->_PKName);$i++) {
                if($i>0) $selectOne .= " AND ";
                $selectOne .= $this->_PKName[$i] ." = :".$this->_PKName[$i];
            }
        } else {
            $selectOne .= "$this->_PKName = :$this->_PKName ";
        }

        $stmt = $this->_connection->prepare($selectOne);
        $stmt->execute(is_array($this->_PKName) ? array_combine($this->_PKName, $ID) : array($this->_PKName=>$ID));

        $stmt->setFetchMode(PDO::FETCH_CLASS, $this->_className);
		$value = $stmt->fetch();
        return $value;
    }

    public function where($field, $op, $value, $concat = "AND"){
        $value = $this->_connection->quote($value);
        if($this->_whereClause == '')
            $this->_whereClause .= "WHERE $field $op $value";
        else
            $this->_whereClause .= (strtoupper($concat) == "AND" ? " AND " : " OR ").  "$field $op $value";
        return $this;
    }
	
	public function groupBy($field) {
		if($this->_groupBy == '')
            $this->_groupBy .= "GROUP BY $field";
        else
            $this->_groupBy .= ", $field ";
        return $this;
	}

    public function orderBy($field, $order = 'ASC'){
        if($this->_orderBy == '')
            $this->_orderBy .= "ORDER BY $field $order";
        else
            $this->_orderBy .= ", $field $order";
        return $this;
    }

	//run select statements
    public function get($addFields = ""){
		if(!empty($addFields))
			$addFields = ",".$addFields;
		
		$select	= "SELECT ".implode(',', $this->getProps(false))."$addFields FROM $this->_className $this->_joinClause $this->_whereClause $this->_groupBy $this->_orderBy";
		//echo "<br> $select <br>";

        $stmt = $this->_connection->prepare($select);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, $this->_className);
        $returnVal = [];
        while($rec = $stmt->fetch()){
            $returnVal[] = $rec;
        }
        return $returnVal;
    }

    public function insert($excludePK = true){
		$properties = $this->getProps($excludePK);
        
		$num = count($properties);
		$insert = '';
		if ($num  > 0){
			$insert 	= 'INSERT INTO ' . $this->_className . '(' . implode(',', $properties) . ') VALUES (:'. implode(',:', $properties) . ')';
		}
        $stmt = $this->_connection->prepare($insert);
        $stmt->execute($this->toArray($properties));
        if(!is_array($this->_PKName)) {
            $pk = $this->_PKName;
            $this->$pk = $this->_connection->lastInsertId();
        }
	}

	public function update(){
		$properties = $this->getProps();
		$num = count($properties);
		$update = '';
		if ($num  > 0){
			//update
			$setClause = [];
			
			foreach($properties as $item)
				$setClause[] = sprintf('%s = :%s', $item, $item);
			$setClause = implode(', ', $setClause);
			$update = 'UPDATE ' . $this->_className . ' SET ' . $setClause . " WHERE ";
		}

        if(is_array($this->_PKName)) {
            for ($i=0;$i<count($this->_PKName);$i++) {
                if($i>0) $update .= " AND ";
                $update .= $this->_PKName[$i] ." = :".$this->_PKName[$i];
            }
        } else {
            $update .= "$this->_PKName = :$this->_PKName ";
            $properties[] = $this->_PKName;
        }

        $stmt = $this->_connection->prepare($update);
        $stmt->execute($this->toArray($properties));
	}

    public function join($type = "", $table, $table1Field, $table2Field = "", $alias = "") {
		if(empty($alias))
			$alias = $table;
		
		$this->_joinClause .= "$type JOIN $table $alias ";
		if(empty($table2Field)) 
			$this->_joinClause .= "using(" . $table1Field . ") ";
		else
			$this->_joinClause .= "on ".$table1Field ."=". $alias .".".$table2Field." ";
        return $this;
    }

	public function delete(){
		$delete = "DELETE FROM $this->_className WHERE ";
        $params = array();

        if(is_array($this->_PKName)) {
            for ($i=0;$i<count($this->_PKName);$i++) {
                if($i>0) $delete .= " AND ";
                $delete .= $this->_PKName[$i] ." = :".$this->_PKName[$i];
                $params[$this->_PKName[$i]] = $this->{$this->_PKName[$i]};
            }
        } else {
            $delete .= "$this->_PKName = :$this->_PKName ";
            $params[$this->_PKName] = $this->{$this->_PKName};
        }
        
        $stmt = $this->_connection->prepare($delete);
        $stmt->execute($params);
	}
}
?>