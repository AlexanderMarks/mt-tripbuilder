<?php

class Controller
{

	public function model($model){
		if(file_exists('models/' . $model . '.php')){
			require_once 'models/' . $model . '.php';
			return new $model();
		}else 
			return getcwd();
	}

}
?>