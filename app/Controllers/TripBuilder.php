<?php
	
class TripBuilder extends Controller {
	
	protected $tripSortFields = array("total_price", "flight_count", "initial_departure");
	
	function getFlights(){
		$flights = $this->model("Flights");
		$flights->join("inner", "airports", "departure_airport", "code", "depap");
		$flights->join("inner", "airports", "arrival_airport", "code", "arrap");
		$flights->orderBy("number");
		
		if(isset($_GET['depart'])) {
			$flights->where("depap.city", "=", $_GET['depart']);
		}
		
		if(isset($_GET['arrive'])) {
			$flights->where("arrap.city", "=", $_GET['arrive']);
		}
		
		if(isset($_GET['departExclude'])) {
			$flights->where("depap.city", "<>", $_GET['departExclude']);
		}
		
		if(isset($_GET['arriveExclude'])) {
			$flights->where("arrap.city", "<>", $_GET['arriveExclude']);
		}
		
		$flights = $flights->get("depap.name as departure_name, depap.city as departure_city, arrap.name as arrival_name, arrap.city as arrival_city");
		
		return json_encode($flights, JSON_UNESCAPED_SLASHES);
	}
	
	//todo sort trip listings
	function getTrips(){
		$trips = $this->model("Trips");
		$trips->join("inner", "flights", "flightid");
		$trips->groupBy("tripid");
		
		if(isset($_GET['sort']) && in_array($_GET['sort'], $this->tripSortFields)) {
			$trips->orderBy($_GET['sort'], (isset($_GET['sortOrder']) && $_GET['sortOrder'] === "desc" ? "DESC" : "ASC"));
		}
		
		$trips = $trips->get("sum(price) total_price, count(flightid) flight_count, min(departure_date) initial_departure");
		foreach($trips as $trip) {
			unset($trip->flightid);
			unset($trip->departure_date);
		}
		return json_encode($trips, JSON_UNESCAPED_SLASHES);
	}
	
	function getTripDetails(){
		if(isset($_GET['id']) && !empty($_GET['id'])) {
			$trips = $this->model("Trips")
				->join("inner", "flights", "flightid")
				->join("inner", "airports", "departure_airport", "code", "depap")
				->join("inner", "airports", "arrival_airport", "code", "arrap")
				->where("tripid", "=", $_GET['id'])
				//->orderBy("CONVERT_TZ(CONVERT(CONCAT(departure_date, ' ', departure_time), DATETIME), departure_timezone, 'Etc/UTC')")
				->orderBy("departure_date")
				->orderBy("departure_time")
				->get("airline, 
						number, 
						depap.name as departure_name, 
						depap.city as departure_city, 
						departure_time, 
						depap.timezone as departure_timezone,
						arrap.name as arrival_name, 
						arrap.city as arrival_city, 
						arrival_time, 
						arrap.timezone as arrival_timezone,
						price");
			
			header('Content-Type: application/json');
			return json_encode($trips, JSON_UNESCAPED_SLASHES);
		} else {
			http_response_code(400);
			echo "missing parameters";
		}
	}
	
	/**
	* departFlight 	id of flight to be added
	* departDate 	date of trip (DD-MM-YYYY)
	*/
	function newOneWayTrip() {
		//departFlight, departDate
		if(isset($_POST['flight']) && !empty($_POST['flight']) 
			&& isset($_POST['date']) && !empty($_POST['date'])) {
			
			
			$flight = $this->getFlightInfo($_POST['flight']);
			if($flight === false) {
				http_response_code(400);
				echo "flight not found";
				exit;
			}
			
			$departDate = strtotime($_POST['date'] . ' '. $flight->departure_time . ' '. $flight->departure_timezone);
			if($departDate < time() || $departDate > (time() + (60*60*24*365))) {
				http_response_code(400);
				echo "invalid departure date";
				exit;
			}
			
			$trip = $this->model("Trips");
			$trip->flightid = $_POST['flight'];
			$trip->departure_date = date('Y-m-d', $departDate);
			$trip->insert();
		} else {
			http_response_code(400);
			echo "missing parameters";
		}
	}
	
	/**
	* departFlight	id of departing flight
	* departDate	date of departing flight (DD-MM-YYYY)
	* returnFlight	id of returning flight
	* returnDate	date of returning flight (DD-MM-YYYY)
	*/
	function newRoundTrip() {
		if(isset($_POST['departFlight']) && !empty($_POST['departFlight']) 
			&& isset($_POST['departDate']) && !empty($_POST['departDate'])
			&& isset($_POST['returnFlight']) && !empty($_POST['returnFlight']) 
			&& isset($_POST['returnDate']) && !empty($_POST['returnDate'])) {
				
			$departFlight = $this->getFlightInfo($_POST['departFlight']);
			if($departFlight === false) {
				http_response_code(400);
				echo "departure flight not found";
				exit;
			}
			
			$departDate = strtotime($_POST['departDate'] . ' '. $departFlight->departure_time . ' '. $departFlight->departure_timezone);
			if($departDate < time() || $departDate > strtotime("+365 days") ) {
				http_response_code(400);
				echo "invalid departure date";
				exit;
			}
			
			$returnFlight = $this->getFlightInfo($_POST['returnFlight']);
			if($returnFlight === false) {
				http_response_code(400);
				echo "return flight  not found";
				exit;
			}
			
			if($returnFlight->departure_city != $departFlight->arrival_city || $returnFlight->arrival_city != $departFlight->departure_city) {
				http_response_code(400);
				echo "invalid return flight";
				exit;
			}
			
			$departArrivalDate = strtotime($_POST['departDate'] . ' ' . $departFlight->arrival_time . ' ' . $departFlight->arrival_timezone);
			if($departDate > $departArrivalDate)
				$departArrivalDate += (60*60*24);
			$returnDate = strtotime($_POST['returnDate'] . ' '. $returnFlight->departure_time . ' '. $returnFlight->departure_timezone);
			
			if($returnDate < $departArrivalDate || $returnDate > ($departArrivalDate + (60*60*24*365))) {
				http_response_code(400);
				echo "invalid return date";
				exit;
			}
			
			$trip = $this->model("Trips");
			$trip->flightid = $_POST['departFlight'];
			$trip->departure_date = date('Y-m-d', $departDate);
			$trip->insert();
			
			$trip->flightid = $_POST['returnFlight'];
			$trip->departure_date = date('Y-m-d', $returnDate);
			$trip->insert(false);
		} else {
			http_response_code(400);
			echo "missing parameters";
		}
	}
	
	/**
	* flights	comma seperated ids of flights to be added
	* dates		comma seperated dates for each flight (DD-MM-YYYY)
	*/
	function newMultiCityTrip() {
		if(isset($_POST['flights']) && !empty($_POST['flights']) 
			&& isset($_POST['dates']) && !empty($_POST['dates'])) {
			
			$flights = $_POST['flights'];
			$dates = $_POST['dates'];
			
			if(!is_array($flights))
				$flights = explode(',',$flights);
			if(!is_array($dates))
				$dates = explode(',',$dates);
			
			if(count($flights) > 5) {
				http_response_code(400);
				echo "too many flights added. maximun 5";
				exit;
			}else if(count($flights) != count($dates)) {
				http_response_code(400);
				echo "number of flights and number of dates do not match";
				exit;
			}
			
			//validate list of flights
			$arrivalDate = time();
			$prevCity = '';
			foreach($flights as $index => $flight) {
				
				$flightInfo = $this->getFlightInfo($flight);
				if($flightInfo === false) {
					http_response_code(400);
					echo "flight " . ($index+1) . " is not found";
					exit;
				}
				
				if(!empty($prevLocation) && $flightInfo->departure_city != $prevLocation) {
					http_response_code(400);
					echo "invalid departure location for flight " . ($index+1);
					exit;
				}
				$prevLocation = $flightInfo->arrival_city;
				
				$departDate = strtotime($dates[$index] . ' '. $flightInfo->departure_time . ' '. $flightInfo->departure_timezone);
				
				if($departDate < $arrivalDate || $departDate > ($arrivalDate + (60*60*24*365)) ) {
					http_response_code(400);
					echo "invalid departure date for flight " . ($index+1);
					exit;
				}
				$arrivalDate = strtotime($dates[$index] . ' ' . $flightInfo->arrival_time . ' ' . $flightInfo->arrival_timezone);
				$dates[$index] = $departDate;
				if($departDate > $arrivalDate)
					$arrivalDate += (60*60*24);
				
			}
			unset($index);
			unset($flight);
			
			$trip = $this->model("Trips");
			foreach ($flights as $index => $flight) {
				$trip->flightid = $flight;
				$trip->departure_date = date('Y-m-d', $dates[$index]);
				$trip->insert($index === 0);
			}
		} else {
			http_response_code(400);
			echo "missing parameters";
		}
	}
	
	/**
	* departFlight	id of departing flight
	* departDate	date of departing flight (DD-MM-YYYY)
	* returnFlight	id of returning flight
	* returnDate	date of returning flight (DD-MM-YYYY)
	*/
	function newOpenJawTrip() {
		if(isset($_POST['departFlight']) && !empty($_POST['departFlight']) 
			&& isset($_POST['departDate']) && !empty($_POST['departDate'])
			&& isset($_POST['returnFlight']) && !empty($_POST['returnFlight']) 
			&& isset($_POST['returnDate']) && !empty($_POST['returnDate'])) {
				
			$departFlight = $this->getFlightInfo($_POST['departFlight']);
			if($departFlight === false) {
				http_response_code(400);
				echo "departure flight not found";
				exit;
			}
			
			$departDate = strtotime($_POST['departDate'] . ' '. $departFlight->departure_time . ' '. $departFlight->departure_timezone);
			if($departDate < time() || $departDate > strtotime("+365 days") ) {
				http_response_code(400);
				echo "invalid departure date";
				exit;
			}
			
			$returnFlight = $this->getFlightInfo($_POST['returnFlight']);
			if($returnFlight === false) {
				http_response_code(400);
				echo "return flight  not found";
				exit;
			}
			
			if($returnFlight->departure_city == $departFlight->arrival_city || $returnFlight->arrival_city != $departFlight->departure_city) {
				http_response_code(400);
				echo "invalid return flight";
				exit;
			}
			
			$departArrivalDate = strtotime($_POST['departDate'] . ' ' . $departFlight->arrival_time . ' ' . $departFlight->arrival_timezone);
			if($departDate > $departArrivalDate)
				$departArrivalDate += (60*60*24);
			$returnDate = strtotime($_POST['returnDate'] . ' '. $returnFlight->departure_time . ' '. $returnFlight->departure_timezone);
			
			if($returnDate < $departArrivalDate || $returnDate > ($departArrivalDate + (60*60*24*365))) {
				http_response_code(400);
				echo "invalid return date";
				exit;
			}
			
			$trip = $this->model("Trips");
			$trip->flightid = $_POST['departFlight'];
			$trip->departure_date = date('Y-m-d', $departDate);
			$trip->insert();
			
			$trip->flightid = $_POST['returnFlight'];
			$trip->departure_date = date('Y-m-d', $returnDate);
			$trip->insert(false);
		} else {
			http_response_code(400);
			echo "missing parameters";
		}
	}
	
	private function getFlightInfo($flightId) {
		$flight = $this->model("Flights")
			->where("flightId", "=", $flightId)
			->join("inner", "airports", "departure_airport", "code", "depap")
			->join("inner", "airports", "arrival_airport", "code", "arrap")
			->get("depap.timezone as departure_timezone, arrap.timezone as arrival_timezone, depap.city as departure_city, arrap.city as arrival_city");
		
		if(empty($flight))
			return false;
		else
			return $flight[0];
	}
}

?>