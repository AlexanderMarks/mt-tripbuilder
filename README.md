# mt-tripbuilder
Mometum travel PHP coding assignment. Sample airport data was retrieved from https://openflights.org/data.html

## Install Instructions

1) download localhost web server software (ex. XAMPP)
2) download or clone repository into the webserver root directory (ex. htdocs)
3) import the database from mt-tripbuilder.sql into a Database Management System (ex. phpMyAdmin/MySQL)
4) edit database connection parameters in /app/core/Model.php if necessary
5) open /demo/index.html on the localhost to view the demo front end application

## Documentation

#### GET /app/getFlights
Retrieves list of flights filtered by parameters

Parameter|Description|Values|Optional
---|---|---|---
depart|Departure flight city name|string|yes
arrive|Arrival flight city name|string|yes
departExclude|City name to be excluded from results|string|yes
arriveExclude|City name to be excluded from results|string|yes
---

#### GET /app/getTrips
Retrieves list of trips sorted by parameters

Parameter|Description|Values|Optional
---|---|---|---
sort|field to be sorted by|total_price, flight_count, initial_departure|yes
sortOrder|direction results should be sorted in|ASC, DESC|yes
---

#### GET /app/getTripDetails
Retrieves list of flights on a trip

Parameter|Description|Values|Optional
---|---|---|---
id|id of trip for which flights are being retrieved|int|no
---

#### POST /app/newOneWayTrip
Creates a new one way trip and stores it in the database

Parameter|Description|Values|Optional
---|---|---|---
flight|id of flight to be added|int|no
date|date of trip|string (dd-mm-yyyy)|no
---

#### POST /app/newRoundTrip
Creates a new round trip and stores it in the database

Parameter|Description|Values|Optional
---|---|---|---
departFlight|id of departing flight|int|no
departDate|date of departing flight|string (dd-mm-yyyy)|no
returnFlight|id of returning flight|int|no
returnDate|date of returning flight|string (dd-mm-yyyy)|no
---

#### POST /app/newMultiCityTrip
Creates a new multi city trip and stores it in the database

Parameter|Description|Values|Optional
---|---|---|---
flights|array or comma seperated string of flight ids to be added|int,array|no
dates|array or comma seperated dates for each flight|string,array (dd-mm-yyyy)|no
---

#### POST /app/newOpenJawTrip
Creates a new open jaw trip and stores it in the database

Parameter|Description|Values|Optional
---|---|---|---
departFlight|id of departing flight|int|no
departDate|date of departing flight|string (dd-mm-yyyy)|no
returnFlight|id of returning flight|int|no
returnDate|date of returning flight|string (dd-mm-yyyy)|no